import { Component, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe("Bolo de Cenoura", "Um bolo bem gosto de cenoura", "https://imagem.band.com.br/novahome/055caa6d-7528-44bc-ac32-3add84bdc0b0.jpg"),
    new Recipe("Bolo de Cenoura", "Um bolo bem gosto de cenoura", "https://imagem.band.com.br/novahome/055caa6d-7528-44bc-ac32-3add84bdc0b0.jpg"),
    new Recipe("Bolo de Cenoura", "Um bolo bem gosto de cenoura", "https://imagem.band.com.br/novahome/055caa6d-7528-44bc-ac32-3add84bdc0b0.jpg"),
    new Recipe("Bolo de Cenoura", "Um bolo bem gosto de cenoura", "https://imagem.band.com.br/novahome/055caa6d-7528-44bc-ac32-3add84bdc0b0.jpg"),
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
